#!/bin/sh
ARCHIVE_PATH='/Users/jmartin/Library/Developer/Xcode/Archives/2014-08-02/Gratitude Attitude 02-08-2014 14.43.xcarchive'
ARCHIVE_PRODUCTS_PATH='/Users/jmartin/Library/Developer/Xcode/Archives/2014-08-02/Gratitude Attitude 02-08-2014 14.43.xcarchive/Products'

MediaAppPath="/Users/jmartin/Dropbox/secondcrack/df/media/apps"
plist="${ARCHIVE_PATH}/Info.plist"

appName=$(/usr/libexec/PlistBuddy -c "Print :Name" "${plist}")
fileAppName=$(echo ${appName}|tr -d ' ')
appVersion=$(/usr/libexec/PlistBuddy -c "Print :ApplicationProperties:CFBundleShortVersionString" "${plist}")
build=$(/usr/libexec/PlistBuddy -c "Print :ApplicationProperties:CFBundleVersion" "${plist}")
appPath=$(/usr/libexec/PlistBuddy -c "Print :ApplicationProperties:ApplicationPath" "${plist}")
bundleId=$(/usr/libexec/PlistBuddy -c "Print :ApplicationProperties:CFBundleIdentifier" "${plist}")

fileName="$fileAppName-$appVersion-build$build"
AppIndexPath="/Users/jmartin/Dropbox/secondcrack/df/pages/apps/$fileAppName/index.md"

TEMPFILE=`mktemp build.XXXXXX`
echo "xcrun -sdk iphoneos PackageApplication \"$ARCHIVE_PRODUCTS_PATH/$appPath\" -o \"${MediaAppPath}/${fileName}.ipa\" --sign \"iPhone Distribution: Joseph Martin (F6J3WTU2J9)\" --embed \"$ARCHIVE_PRODUCTS_PATH/$appPath/embedded.mobileprovision\" >/Users/jmartin/build.log 2>/Users/jmartin/build.err" > "$TEMPFILE"
/bin/bash -l "$TEMPFILE"
rm "$TEMPFILE"

distPlist="${MediaAppPath}/${fileName}.plist"
cp "/Users/jmartin/development/iOS Apps/templates/AppTemplate.plist" "${distPlist}"
sed -i "" "s/AppFileName/${fileName}.ipa/" "${distPlist}"
sed -i "" "s@AppBundleId@${bundleId}@" "${distPlist}"
sed -i "" "s@AppBundleVersion@${appVersion}.${build}@" "${distPlist}"
sed -i "" "s@AppTitle@${appName}@" "${distPlist}"

read -d '' snippet <<- EOF
#### [build ${build}](itms-services://?action=download-manifest&amp;url=https://desertflood.com/media/apps/${fileName}.plist)

**Changes**

* 
EOF
echo "${snippet}" | pbcopy
open "${AppIndexPath}"
